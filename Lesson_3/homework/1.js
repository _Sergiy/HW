/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

var OurSliderImages = [
  "images/cat1.jpg",
  "images/cat2.jpg",
  "images/cat3.jpg",
  "images/cat4.jpg",
  "images/cat5.jpg",
  "images/cat6.jpg",
  "images/cat7.jpg",
  "images/cat8.jpg"
];
var currentPosition = 0;

var slider = document.querySelector("#slider");
var prev = document.querySelector("#PrevSilde");
var next = document.querySelector("#NextSilde");

function animate() {
  var img = slider.querySelector("img");
  setTimeout(function() {
    img.classList.add("img");
  }, 300);
}

function firstSlide() {
  var slide = document.createElement("img");
  slide.src = OurSliderImages[currentPosition];
  slider.appendChild(slide);
  animate();
}

function RenderImage(a, b) {
  slider.innerHTML = "";
  console.log(currentPosition);

  var slide = document.createElement("img");

  if (b == "next") {
    if (currentPosition < OurSliderImages.length - 1) {
      currentPosition = currentPosition + 1;
      slide.src = OurSliderImages[a];
      slider.appendChild(slide);
      animate();
    } else {
      slide.src = OurSliderImages[0];
      currentPosition = 0;
      slider.appendChild(slide);
      animate();
    }
  } else if (b == "prev") {
    if (currentPosition > 0) {
      currentPosition = currentPosition - 1;
      slide.src = OurSliderImages[a];
      slider.appendChild(slide);
      animate();
    } else {
      slide.src = OurSliderImages[OurSliderImages.length - 1];
      currentPosition = OurSliderImages.length - 1;
      slider.appendChild(slide);
      animate();
    }
  }
}

function prevSlide() {
  prev.addEventListener("click", function() {
    RenderImage(currentPosition - 1, "prev");
  });
}
function nextSlide() {
  next.addEventListener("click", function() {
    RenderImage(currentPosition + 1, "next");
  });
}

document.addEventListener("DOMContentLoaded", function() {
  firstSlide();
  prevSlide();
  nextSlide();
});
