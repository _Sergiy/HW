/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
var Train = {};
Train.name = "Kyiv - Odessa";
Train.speed = 70;
Train.numbers = 1000;
Train.run = function() {
  console.log(
    "Поезд " +  this.name + " везет " + this.numbers + " пассажиров " + " со скоростью " + this.speed
  );
};

Train.stay = function(  ) {
    this.speed = 0;
console.log("Поезд " +
    this.name + " остановился. Скорость " + this.speed );
};
Train.passangers = function(pass){
    this.numbers = this.numbers + pass;
    console.log("подобрать пассажиров " + this.numbers);
}

Train.run();
Train.stay();
Train.passangers(100);
console.log(Train);
