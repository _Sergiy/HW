/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
window.addEventListener('DOMContentLoaded', () => {
	let klient = 'http:www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
	let klientFriends = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';

	let resJson = result => result.json();
	let randomKlient = klientsList =>
		klientsList[Math.floor(Math.random() * klientsList.length)];
	let klientFriend = klient => {
		let klientAddFriends = klientFriendsList => ({
			name: klient.name,
			gender: klient.gender,
			age: klient.age,
			id: klientFriendsList[0]._id,
			friends: klientFriendsList[0].friends
		});
		return fetch(klientFriends).then(resJson).then(klientAddFriends);
	};

	let klientR = klient => {
		let klientRd = `
			<h2>${klient.name}</h2>
			<h5>${klient.gender}, ${klient.age}</h5>
			<h5>ID: ${klient.id}</h5>           
			Friends:
			<ul>
			${klient.friends.map(friends => `<li>${friends.name}</li>`)}
			</ul>
		`;
		let div = document.createElement('div');
		div.innerHTML = klientRd;
		document.body.appendChild(div);
	};

	fetch(klient).then(resJson).then(randomKlient).then(klientFriend).then(klientR);
});
