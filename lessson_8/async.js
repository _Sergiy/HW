			/*

				Задание:

		Написать при помощи async-await скрипт, который:
		Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
		Перебирает, выводит табличку:
		Company | Balance | Показать дату регистрации | Показать адресс |
		1. CompName 2000$ button button
		Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.
		*/
			window.addEventListener("DOMContentLoaded", () => {
			let companies = getCompanyList();
			companies.then(getTable);

			async function getCompanyList() {
				const getCompany = await fetch(
				"http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2"
				);
				const companies = await getCompany.json();
				return companies;
			}

			function getTable(companies) {
				let table = document.createElement("table");

				let header = document.createElement("tr");

				header.innerHTML =
				"<th>Company</th><th>Balance</th><th>Registered</th><th>Address</th>";

				table.appendChild(header);

				companies.forEach(company => {
				let tr = document.createElement("tr");

				getNameBalance(company.company, tr);
				getNameBalance(company.balance, tr);
				buttons(company.registered, tr);
				buttons(addressToString(company.address), tr);

				table.appendChild(tr);
				});

				document.body.appendChild(table);
			}
			function getNameBalance(data, tr) {
				let td = document.createElement("td");
				td.innerHTML = data;
				tr.appendChild(td);
			}
			function buttons(data, tr) {
				let td = document.createElement("td");
				let span = document.createElement("span");
				span.innerHTML = data;
				span.classList.add("visible");				
				td.appendChild(span);
				tr.appendChild(td);

				let button = document.createElement("button");
				button.innerHTML = "Try This!!!";
				button.addEventListener("click", function(e) {
				this.classList.toggle("visible");
					this.previousSibling.classList.toggle("visible");
				});
				td.appendChild(button);
			}
			function addressToString(address) {
				return `${address.zip}, ${address.country}, ${address.state}, ${address.city}, ${address.street}, ${address.house}`;
			}
			});
