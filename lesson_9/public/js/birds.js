/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./classwork/task1.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./classwork/Eating.js":
/*!*****************************!*\
  !*** ./classwork/Eating.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction Eating() {\r\n    console.log(`${this.name} is  Eating`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Eating);\n\n//# sourceURL=webpack:///./classwork/Eating.js?");

/***/ }),

/***/ "./classwork/Hunting.js":
/*!******************************!*\
  !*** ./classwork/Hunting.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction Hunting() {\r\n    console.log(`${this.name} is  Hunting`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Hunting);\r\n\n\n//# sourceURL=webpack:///./classwork/Hunting.js?");

/***/ }),

/***/ "./classwork/Ride.js":
/*!***************************!*\
  !*** ./classwork/Ride.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction Ride() {\r\n    console.log(`${this.name} is  Ride`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Ride);\n\n//# sourceURL=webpack:///./classwork/Ride.js?");

/***/ }),

/***/ "./classwork/Singing.js":
/*!******************************!*\
  !*** ./classwork/Singing.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction Singing() {\r\n    console.log(`${this.name} is  Singing`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Singing);\r\n\n\n//# sourceURL=webpack:///./classwork/Singing.js?");

/***/ }),

/***/ "./classwork/TransferMail.js":
/*!***********************************!*\
  !*** ./classwork/TransferMail.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction TransferMail() {\r\n    console.log(`${this.name} is   Transfer mail`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (TransferMail);\r\n\n\n//# sourceURL=webpack:///./classwork/TransferMail.js?");

/***/ }),

/***/ "./classwork/fly.js":
/*!**************************!*\
  !*** ./classwork/fly.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\r\nfunction fly(){\r\n    console.log(`${this.name} is flying`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (fly);\n\n//# sourceURL=webpack:///./classwork/fly.js?");

/***/ }),

/***/ "./classwork/run.js":
/*!**************************!*\
  !*** ./classwork/run.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction run(){\r\n    console.log(`${this.name} is run`);\r\n}\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (run);\n\n//# sourceURL=webpack:///./classwork/run.js?");

/***/ }),

/***/ "./classwork/swim.js":
/*!***************************!*\
  !*** ./classwork/swim.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction swim() {\r\n  console.log(`${this.name} is swim`);\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (swim);\r\n\n\n//# sourceURL=webpack:///./classwork/swim.js?");

/***/ }),

/***/ "./classwork/task1.js":
/*!****************************!*\
  !*** ./classwork/task1.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _fly__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fly */ \"./classwork/fly.js\");\n/* harmony import */ var _run__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./run */ \"./classwork/run.js\");\n/* harmony import */ var _swim__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./swim */ \"./classwork/swim.js\");\n/* harmony import */ var _Eating__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Eating */ \"./classwork/Eating.js\");\n/* harmony import */ var _Hunting__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Hunting */ \"./classwork/Hunting.js\");\n/* harmony import */ var _Singing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Singing */ \"./classwork/Singing.js\");\n/* harmony import */ var _Ride__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Ride */ \"./classwork/Ride.js\");\n/* harmony import */ var _TransferMail__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./TransferMail */ \"./classwork/TransferMail.js\");\n/*\n\n  Задание:\n    Написать конструктор обьекта. Отдельные функции разбить на модули\n    и использовать внутри самого конструктора.\n    Каждую функцию выделить в отдельный модуль и собрать.\n\n    Тематика - птицы.\n    Птицы могут:\n      - Нестись\n      - Летать\n      - Плавать\n      - Кушать\n      - Охотиться\n      - Петь\n      - Переносить почту\n      - Бегать\n\n  Составить птиц (пару на выбор, не обязательно всех):\n      Страус\n      Голубь\n      Курица\n      Пингвин\n      Чайка\n      Ястреб\n      Сова\n\n */\n\n\n\n\n\n\n\n\n\n\nclass Chicken {\n  constructor(name) {\n    this.name = name;\n    this.run = _run__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\n    this.Ride = _Ride__WEBPACK_IMPORTED_MODULE_6__[\"default\"];\n    this.fly = _fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n  }\n}\nlet Chik= new Chicken(\"Test\");\nChik.fly();\nChik.run();\n\nclass Pingvin {\n  constructor(name) {\n    this.name = name;\n    this.run = _run__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\n    this.swim = _swim__WEBPACK_IMPORTED_MODULE_2__[\"default\"];\n  }\n}\nlet Penguin = new Pingvin(\"Sunny\");\nPenguin.run();\nPenguin.swim();\n\nclass Dove {\n  constructor(name) {\n    this.name = name;\n    this.fly = _fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.TransferMail = _TransferMail__WEBPACK_IMPORTED_MODULE_7__[\"default\"];\n  }\n}\nlet Dover = new Dove(\"Golub\");\nDover.fly();\nDover.TransferMail();\n\nclass Gull {\n  constructor(name) {\n    this.name = name;\n    this.fly = _fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.Eating = _Eating__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n    this.Singing = _Singing__WEBPACK_IMPORTED_MODULE_5__[\"default\"];\n  }\n}\nlet Gulls = new Gull(\"Chayka\");\nGulls.fly();\nGulls.Eating();\n\nclass Hawk {\n  constructor(name) {\n    this.name = name;\n    this.fly = _fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.Hunting = _Hunting__WEBPACK_IMPORTED_MODULE_4__[\"default\"];\n    this.Eating = _Eating__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n  }\n}\nlet Hawks = new Hawk(\"YAstreb\");\nHawks.fly();\nHawks.Eating();\nHawks.Hunting();\n\nclass Owl {\n  constructor(name) {\n    this.name = name;\n    this.fly = _fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.Eating = _Eating__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n  }\n}\nlet Owls = new Owl(\"SOva\");\nOwls.fly();\nOwls.Eating();\n\n\n//# sourceURL=webpack:///./classwork/task1.js?");

/***/ })

/******/ });