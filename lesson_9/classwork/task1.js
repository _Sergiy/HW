/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */

import fly from "./fly";
import run from "./run";
import swim from "./swim";
import Eating from "./Eating";
import Hunting from "./Hunting";
import Singing from "./Singing";
import Ride from "./Ride";
import TransferMail from "./TransferMail";

class Chicken {
  constructor(name) {
    this.name = name;
    this.run = run;
    this.Ride = Ride;
    this.fly = fly;
  }
}
let Chik= new Chicken("Test");
Chik.fly();
Chik.run();

class Pingvin {
  constructor(name) {
    this.name = name;
    this.run = run;
    this.swim = swim;
  }
}
let Penguin = new Pingvin("Sunny");
Penguin.run();
Penguin.swim();

class Dove {
  constructor(name) {
    this.name = name;
    this.fly = fly;
    this.TransferMail = TransferMail;
  }
}
let Dover = new Dove("Golub");
Dover.fly();
Dover.TransferMail();

class Gull {
  constructor(name) {
    this.name = name;
    this.fly = fly;
    this.Eating = Eating;
    this.Singing = Singing;
  }
}
let Gulls = new Gull("Chayka");
Gulls.fly();
Gulls.Eating();

class Hawk {
  constructor(name) {
    this.name = name;
    this.fly = fly;
    this.Hunting = Hunting;
    this.Eating = Eating;
  }
}
let Hawks = new Hawk("YAstreb");
Hawks.fly();
Hawks.Eating();
Hawks.Hunting();

class Owl {
  constructor(name) {
    this.name = name;
    this.fly = fly;
    this.Eating = Eating;
  }
}
let Owls = new Owl("SOva");
Owls.fly();
Owls.Eating();
