/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
function getRandomIntInclusive(min, max) {
  var min = Math.ceil(min);
  var max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
// 1 способ
// color = [];
// for (i=0; i<3; i++){
//   var rgbcolor = getRandomIntInclusive(0,255);
//   color.push(rgbcolor)
// }

// color = color.join(",");
// console.log(color);
// var y = "rgb("+ color +")";
// document.body.style.background = y;

// 2 способ
color = [];
for (i=0; i<3; i++){
  var sixteencolor = getRandomIntInclusive(0,255).toString(16);
  color.push(sixteencolor)
}
var y = "#"+color[0]+color[1]+color[2]+"";
document.body.style.background = y;
console.log(color);
console.log(y);

// выводиться в диве отдельно от боди
var btn = document.createElement('button');
btn.innerText = "Жми сюда";
var app = document.getElementById('app');
app.style.height = '500px';
app.style.width = '500px';
app.style.border = '3px solid black';
app.style.margin = '400px';
app.appendChild(btn);
btn.addEventListener('click', function(){
  color = [];
for (i=0; i<3; i++){
  var rgbcolor = getRandomIntInclusive(0,255);
  color.push(rgbcolor)
}
color = color.join(",");
var y = "rgb("+ color +")";
app.style.background = y;
})
